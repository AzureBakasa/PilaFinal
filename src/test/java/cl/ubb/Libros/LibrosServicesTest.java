package cl.ubb.Libros;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import static org.mockito.Matchers.anyLong;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.LibrosDao;
import cl.ubb.model.Libro;
import cl.ubb.service.LibrosService;

@RunWith(MockitoJUnitRunner.class)

public class LibrosServicesTest {
	/*Notas:
	 * Then return le pasas la variable donde recives, when simula una condicion, simula la operacion con un mock
	 * 	luego el arraylist es la variable de control que se espera obtener 
	 */
	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService;
	
	@Test
	public void deboCrearLibroYRetornarlo(){
		//arrange
		Libro libro = new Libro();
		libro.setId(1L);
		//act
		when(librosDao.save(libro)).thenReturn(libro);
		Libro libroCreado=librosService.crearLibro(libro);
		//assert
		assertNotNull(libroCreado);
		assertEquals(libroCreado,libro);
	}
	
	@Test
	public void buscarLibroPorId(){
		//arrange
		Libro libro = new Libro();
		libro.setId(1L);		
		//act
		when(librosDao.findOne(anyLong())).thenReturn(libro);
		Libro libroRetornado=librosService.obtenerLibro(libro.getId());
		//assert
		assertNotNull(libroRetornado);
	}
	@Test
	public void obtenerTodosLosLibros(){
		//arrange
		ArrayList<Libro> misLibros = new ArrayList<Libro>();
		//act
		when(librosDao.findAll()).thenReturn(misLibros);
		ArrayList<Libro> resultado=librosService.obtenerTodosLosLibros();
		//assert
		assertNotNull(resultado);
		assertEquals(misLibros,resultado);
	}
	

}
