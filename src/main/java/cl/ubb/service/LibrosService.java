package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.LibrosDao;
import cl.ubb.model.Libro;

public class LibrosService {

	private LibrosDao libroDao;
	@Autowired
	public Libro crearLibro(Libro libro) {
		return libroDao.save(libro);
	}
	public Libro obtenerLibro(Long id) {
		Libro libro=new Libro();
		libro=libroDao.findOne(id);
		return libro;
	}
	/*public Libro obtenerLibro(Long id) {//minima codigo posible
		Libro libro=new Libro();
		return libro;
	}*/
	public ArrayList<Libro> obtenerTodosLosLibros() {		
		return (ArrayList<Libro>) libroDao.findAll();
	}

}
